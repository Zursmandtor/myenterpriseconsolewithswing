package com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.employee.impl;


import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.employee.Employee;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * Type of employee manager
 *
 * @author Rudenko Ievgen
 */
@Entity
@DiscriminatorValue("ManDisc")
public class Manager extends Employee implements Serializable {

    /**
     * Creates employee using name and age
     *
     * @param name name of the employee
     * @param age  of employee
     */
    public Manager(int id, String name, int age, int typeOfEmployee) {
        super(id, name, age, typeOfEmployee);
    }

    public Manager() {
    }
}
