package com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.impl;

import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.department.Department;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.Enterprise;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Type of enterprise mining enterprise
 *
 * @author Rudenko Ievgen
 */
@Entity
@DiscriminatorValue("MinDisc")
public class MiningEnterprise extends Enterprise implements Serializable {

    /**
     * Creates enterprise using name and list of department
     *
     * @param name           name of the enterprise
     * @param departmentList list of department
     */
    public MiningEnterprise(int id, String name, List<Department> departmentList, Integer type) {
        super(id, name, departmentList, type);
    }

    /**
     * Creates mining enterprise without parameters
     */
    public MiningEnterprise (){

    }
}
