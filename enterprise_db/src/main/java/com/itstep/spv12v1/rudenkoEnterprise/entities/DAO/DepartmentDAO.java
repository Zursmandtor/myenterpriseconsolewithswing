package com.itstep.spv12v1.rudenkoEnterprise.entities.DAO;


import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.department.Department;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.Enterprise;
import com.itstep.spv12v1.rudenkoEnterprise.utils.HibernateUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to perform operations with the departments in the database
 * @author Rudenko Ievgen
 */
public class DepartmentDAO {

    /**
     * Create logger.
     */
    private static final Logger log = Logger.getLogger(DepartmentDAO.class);

    /**
     * Database connection manager
     */
    private HibernateUtils hibernateUtils;


    /**
     * Creates instance using connection manager
     *
     * @param hibernateUtils hibernate utils instance
     */
    public DepartmentDAO(HibernateUtils hibernateUtils) {
        this.hibernateUtils = hibernateUtils;
    }

    /**
     * Find all departments in the enterprise
     *
     * @return list of department
     */
    public List<Department> findAll() {
        List<Department> departments = new ArrayList();
        try {
            Session session = hibernateUtils.getSession();
            departments = (List<Department>) session.createQuery("from Department")
                    .list();
            return departments;
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Department wasn`t found", he);
        }
        return departments;
    }

    /**
     * Find department in the selected enterprise
     *
     * @return list of department
     */
    public List<Department> findByEnterprise(Enterprise enterprise) {
        List<Department> departments = new ArrayList();
        try {
            Session session = hibernateUtils.getSession();
            departments = (List<Department>) session.createQuery("from Department as dep where " +
                    "(:enterprise) in elements(dep.employeeList) ")
                    .setEntity("enterprise", enterprise)
                    .list();
            return departments;
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Department wasn`t found", he);
        }
        return departments;
    }

    /**
     * Find department by id
     *
     * @param id department`s id
     * @return department
     */
    public Department findById(Integer id) {
        Department department = null;
        try {
            Session session = hibernateUtils.getSession();
            department = (Department) session.get(Department.class, new Integer(id));

            return department;
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Department wasn`t found", he);
        }
        return department;
    }

    /**
     * Find department by name.
     *
     * @param name department`s name
     * @return list of departments
     */
    public List<Department> findByName(String name) {
        List<Department> employees = null;
        try {
            Session session = hibernateUtils.getSession();
            employees = (List<Department>) session.get(Department.class, new String(name));
            return employees;
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Department wasn`t found", he);
        }
        return employees;
    }

    /**
     * Save new department
     *
     * @param department new department
     * @return new department
     */
    public boolean saveDepartment(Department department) {
        int result = 0;
        try {
            Session session = hibernateUtils.getSession();

            session.beginTransaction();
            session.save(department);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Department wasn`t saved", he);

        }
        return result == 1;
    }


    /**
     * Save new department
     *
     * @param department new department
     * @return true, if department saved
     */
    public boolean saveDepartmentToEnterprise(Enterprise enterprise, Department department) {
        try {
            enterprise.getDepartmentList().add(department);
            Session session = hibernateUtils.getSession();
            session.beginTransaction();
            session.saveOrUpdate(enterprise);
            session.save(department);
            session.getTransaction().commit();

            return true;
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Department wasn`t saved", he);
            return false;
        }
    }


    /**
     * Delete department
     *
     * @param department deleting department
     * @return true, if department deleted
     */
    public boolean removeDepartment(Department department) {
        int result = 0;
        try {
            Session session = hibernateUtils.getSession();
            session.beginTransaction();
            session.delete(department);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            log.debug("Department was deleted");
        }
        return result == 1;
    }


}


