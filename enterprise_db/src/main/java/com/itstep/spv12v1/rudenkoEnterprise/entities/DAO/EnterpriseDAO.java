package com.itstep.spv12v1.rudenkoEnterprise.entities.DAO;



import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.Enterprise;
import com.itstep.spv12v1.rudenkoEnterprise.utils.HibernateUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to perform operations with the enterprises in the database
 * @author Rudenko Ievgen
 */
public class EnterpriseDAO {

    /**
     * Create logger.
     */
    private static final Logger log = Logger.getLogger(EnterpriseDAO.class);

    /**
     * Database connection manager
     */
    private HibernateUtils hibernateUtils;

    /**
     * Creates instance using connection manager
     *
     * @param hibernateUtils hibernate utils instance
     */
    public EnterpriseDAO(HibernateUtils hibernateUtils) {
        this.hibernateUtils = hibernateUtils;
    }


    /**
     * Find all enterprises
     *
     * @return list of enterprises
     */
    public List<Enterprise> findAll() {
        try {
            Session session = hibernateUtils.getSession();
            log.debug("Enterprises was read from database.");
            return session.createQuery("from Enterprise").list();
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Enterprises was not read from database successfully", he);
            return new ArrayList();
        }
    }

    /**
     * Find enterprise by id
     *
     * @param id eneterprise`s id
     * @return enterprise
     */
    public Enterprise findById(Integer id) {
        Enterprise enterprise = new Enterprise();
        try {
            Session session = hibernateUtils.getSession();
            enterprise = (Enterprise) session.get(Enterprise.class, new Integer(id));
            log.debug("Enterprise was found");
            return enterprise;
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Enterprise wasn`t found", he);
        }
        return enterprise;
    }

    /**
     * Find enterprise by name.
     *
     * @param name department`s name
     * @return list of departments
     */
    public Enterprise findByName(String name) {
        Enterprise enterprise = new Enterprise();
        try {
            Session session = hibernateUtils.getSession();
            enterprise = (Enterprise) session.get(Enterprise.class, new String(name));
            log.debug("Enterprise was found");
            return enterprise;
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Enterprise wasn`t found", he);
        }
        return enterprise;
    }

    /**
     * Save new enterprise.
     * @param enterprise new enterprise
     * @return true, if enterprise saved
     */
    public boolean saveEnterprise(Enterprise enterprise) {
        int result = 0;
        try {
            Session session = hibernateUtils.getSession();

            session.beginTransaction();
            session.save(enterprise);
            session.getTransaction().commit();
            log.debug("Enterprise was saved");
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Enterprise wasn`t saved", he);
        }
        return result == 1;
    }

    /**
     * Delete enterprise
     *
     * @param enterprise deleting enterprise
     * @return true, if enterprise deleted
     */
    public boolean removeEnterprise(Enterprise enterprise) {
        int result = 0;
        try {
            Session session = hibernateUtils.getSession();
            session.beginTransaction();
            session.delete(enterprise);
            session.getTransaction().commit();
            log.debug("Enterprise was deleted");
        } catch (HibernateException he) {
            he.printStackTrace();
            log.debug("Enterprise wasn`t deleted", he);
        }
        return result == 1;
    }
}