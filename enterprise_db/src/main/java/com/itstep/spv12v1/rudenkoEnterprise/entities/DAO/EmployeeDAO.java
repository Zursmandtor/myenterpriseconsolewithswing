package com.itstep.spv12v1.rudenkoEnterprise.entities.DAO;


import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.department.Department;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.employee.Employee;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.Enterprise;
import com.itstep.spv12v1.rudenkoEnterprise.utils.HibernateUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Class to perform operations with the employees in the database
 * @author Rudenko Ievgen
 */
public class EmployeeDAO {

    /**
     * Create logger.
     */
    private static final Logger log = Logger.getLogger(EmployeeDAO.class);

    /**
     * Database connection manager
     */
    private HibernateUtils hibernateUtils;

    /**
     * Creates instance using connection manager
     *
     * @param hibernateUtils hibernate utils instance
     */
    public EmployeeDAO(HibernateUtils hibernateUtils) {
        this.hibernateUtils = hibernateUtils;
    }

    /**
     * Find employee by id
     * @param id employee`s id
     * @return employee
     */
    public Employee findById(Long id){
        Employee employee = null;
        try{
        Session session  = hibernateUtils.getSession();
        employee = (Employee) session.get(Employee.class, new Long(id));
        log.debug("Employee founded");
        return employee;
        } catch (HibernateException he){
            he.printStackTrace();
            log.error("Employee wasn`t found", he);
        }
        return employee;
    }

    /**
     * Find employee by name.
     * @param name employee`s name
     * @return list of employees
     */
    public List<Employee> findByName(String name){
        List<Employee> employees = null;
        try {
            Session session = hibernateUtils.getSession();
            employees = (List<Employee>) session.get(Department.class, new String(name));
            log.debug("Employee founded");
            return employees;
        } catch (HibernateException he){
            he.printStackTrace();
            log.error("Employee wasn`t found", he);
        }
        return employees;
    }

    /**
     * Find employee by age.
     * @param age employee`s age
     * @return list of  employees
     */
    public List<Employee> findByAge(Integer age){
        List<Employee> employees = null;
        try {
            Session session = hibernateUtils.getSession();
            employees = (List<Employee>) session.get(Department.class, new Integer (age));
            log.debug("Employee founded");
            return employees;
        } catch (HibernateException he){
            he.printStackTrace();
            log.error("Employee wasn`t found", he);
        }
        return employees;
    }

    /**
     * Find all employees
     * @return list of employees
     */
    public Set<Employee> findAll(){
        Set<Employee> employeess = new HashSet();
        try {
            Session session = hibernateUtils.getSession();
            employeess = (Set<Employee>) session.createQuery ("from Employee")
                    .list();
            log.debug("Employee founded");
            return employeess;
        } catch (HibernateException he){
            he.printStackTrace();
            log.error("Employees wasn`t found", he);
        }
        return employeess;
    }

    /**
     * Save new employee.
      * @param employee new employee
     * @return true, if employee saved
     */
    public boolean saveEmployee(Employee employee) {
        int result = 0;
        try {
            Session session = hibernateUtils.getSession();
            session.beginTransaction();
            session.save(employee);
            session.getTransaction().commit();
            log.debug("Employee was saved");
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Employee wasn`t saved", he);

        }
        return result == 1;
    }

    /**
     * Save new employee to the selection department and the selection enterprise
     * @param employee new employee
     * @param enterprise selection enterprise
     * @param department selection department
     * @return true, if employee saved
     */
    public boolean saveEmployeeToDepartment(Enterprise enterprise, Department department, Employee employee) {
        int result = 0;

        try {
            Session session = hibernateUtils.getSession();

            session.beginTransaction();
            session.save(employee);
            session.save(department);
            session.save(enterprise);
            department.getEmployeeList().add(employee);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Employee wasn`t saved", he);

        }
        return result == 1;
    }

    /**
     * Delete employee
     * @param employee deleting employee
     * @return true, if employee deleted
     */
    public boolean removeEmployee (Employee employee){
        int result = 0;
        try{
            Session session = hibernateUtils.getSession();
            session.beginTransaction();
            session.delete(employee);
            session.getTransaction().commit();
        } catch (HibernateException he){
            he.printStackTrace();
            log.debug("Employee wasn`t deleted");
        }
        return result == 1;
    }

    /**
     * Find all employees in the department
     * @return list of employees
     */
    public List<Employee> findByDepartment(Department department) {
        List<Employee> employees = new ArrayList<Employee>();
        try {
            return department.getEmployeeList();
        } catch (HibernateException he) {
            he.printStackTrace();
            log.error("Employee wasn`t found", he);
        }
        return employees;
    }




}
