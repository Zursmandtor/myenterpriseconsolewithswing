package com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.employee;

import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.department.Department;

import javax.persistence.*;
import java.util.List;

/**
 * Employee entity
 *
 * @author Rudenko Ievgen
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="disc", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("EmpInh")
public class Employee {


    /**
     * Employee's id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Employee's name
     */
    @Column(name="name")
    private String name;

    /**
     * Employee's age
     */
    private int age;
    /**
     * Employee`s type
     */
    @Column (name="type")
    private Integer typeOfEmployee;

    /**
     * Employees that work in the department.
     */
    @ManyToMany (mappedBy="employeeList")
    private List<Department> departmentList;

    public List<Department> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }

    /**
     * Creates new employee based on name and age arguments.
     *
     * @param id   Employee's id
     * @param name Employee's name
     * @param age  Employee's age
     */
    public Employee(int id, String name, int age, int typeOfEmployee) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.typeOfEmployee = typeOfEmployee;

    }

    /**
     * Creates new employee with id = 0, 'Undefined' name and, age of 25, type 'Undefined'
     */
    public Employee() {
        this(0, "Undefined", 25, 0);
    }


    /**
     * Sets id to the employee
     *
     * @param id new employee's id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Sets name to the employee
     *
     * @param name new employee's name
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Sets age to the employee
     *
     * @param age new employee's age
     */
    public void setAge(int age) {
        this.age = age;
    }


    /**
     * Returns employee`s id
     *
     * @return employee`s id
     */
    public int getId() {
        return id;
    }


    /**
     * Returns employee`s age
     *
     * @return employee`s age
     */
    public int getAge() {
        return age;
    }


    /**
     * Returns employee`s name
     *
     * @return employee`s name
     */
    public String getName() {
        return name;
    }




    /**
     * String representation of an employee
     *
     * @return string representation of an employee
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Compares employee and an object
     *
     * @param o object to be compared with
     * @return true if o is an instance of Employee class and has the same name and age
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (age != employee.age) return false;
        if (!name.equals(employee.name)) return false;

        return true;
    }

    /**
     * hashCode implementation
     *
     * @return hash code of an instance
     */
    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + age;
        return result;
    }
}
