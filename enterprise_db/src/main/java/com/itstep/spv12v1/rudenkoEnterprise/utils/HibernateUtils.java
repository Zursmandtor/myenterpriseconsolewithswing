package com.itstep.spv12v1.rudenkoEnterprise.utils;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;


public class HibernateUtils {

    private SessionFactory sessionFactory;
    private Session session;

    public HibernateUtils(){
        createSessionFactory();
    }

    private void createSessionFactory() {
        ServiceRegistry serviceRegistry;
        Configuration configuration = new Configuration().configure();
        serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }

    public Session getSession(){
       if (session == null || ! session.isOpen()){
           session=sessionFactory.openSession();
       }
        return session;
    }

    public void closeSession(){
        if(session != null || session.isOpen()){
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

}
