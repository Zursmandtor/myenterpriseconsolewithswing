package com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.impl;


import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.department.Department;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.Enterprise;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Type of enterprise music store
 *
 * @author Rudenko Ievgen
 */
@Entity
@DiscriminatorValue("MusDisc")
public class MusicStore extends Enterprise implements Serializable {

    /**
     * Creates enterprise using name and list of department
     *
     * @param name           name of the enterprise
     * @param departmentList list of department
     */
    public MusicStore(int id, String name, List<Department> departmentList, Integer type) {
        super(id, name, departmentList, type);
    }

    /**
     * Creates music store without parameters
     */
    public MusicStore(){

    }

}
