package com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise;

import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.department.Department;
import org.apache.log4j.Logger;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Enterprise entity
 *
 * @author Rudenko Ievgen
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn (name="disc",discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("EntInh")
public class Enterprise {

    private static final Logger log = Logger.getLogger(Enterprise.class);

    /**
     * Enterprise's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    /**
     * Enterprise's name.
     */
    @Column(name="name")
    private String name;


    /**
     * Departments that are contained in this enterprise.
     */
    @OneToMany(fetch= FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinColumn(name="enterprise_id")
    private List<Department> departmentList = new ArrayList();

    /**
     * Type of enterprise
     */
    @Transient
    public Integer typeOfEnterprise;

    /**
     * Creates Enterprise instance with specified name and departments list
     *
     * @param name enterprise's name
     */
    public Enterprise(Integer id, String name, List<Department> departmentList, Integer typeOfEnterprise) {
        this.id = id;
        this.name = name;
        this.departmentList = departmentList;
        this.typeOfEnterprise = typeOfEnterprise;
    }

    /**
     * Creates Enterprise instance with specified name
     *
     * @param name enterprise's name
     * @param typeOfEnterprise enterprise`s type
     */
    public Enterprise(String name, Integer typeOfEnterprise) {
        this.name = name;
        this.typeOfEnterprise = typeOfEnterprise;
    }

    public Enterprise() {
    }

    /**
     * Sets the id to the enterprise
     *
     * @param id new enterprise's name
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Sets the name to the enterprise
     *
     * @param name new enterprise's name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns enterprise's id
     *
     * @return enterprise's id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Returns enterprise's name
     *
     * @return enterprise's name
     */
    public String getName() {
        return name;
    }


    /**
     * Sets department`s list of the enterprise
     *
     * @param departmentList all departments of teh enterprise
     */
    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }

    /**
     * return department`s list of the enterprise
     *
     * @return department`s list
     */
    public List<Department> getDepartmentList() {
        if(departmentList==null){
            departmentList = new ArrayList<Department>();
        }
        return departmentList;
    }


    /**
     * Return enterprise's type
     *
     * @return enterprise's type
     */
    public Integer getTypeOfEnterprise() {
        return typeOfEnterprise;
    }


    /**
     * Set type of enterprise
     *
     * @param typeOfEnterprise enterprise`s type
     */
    public void setTypeOfEnterprise(Integer typeOfEnterprise) {
        this.typeOfEnterprise = typeOfEnterprise;
    }

    /**
     * String representation of an enterprise
     *
     * @return string representation of an enterprise
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(name.toUpperCase());

        return stringBuilder.toString();
    }

    /**
     * Compares department and an object
     *
     * @param o object to be compared with
     * @return true if o is an instance of Department class and has the same name and list of employees
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Enterprise that = (Enterprise) o;

        if (!departmentList.equals(that.departmentList)) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    /**
     * HashCode implementation
     *
     * @return hash code of an instance
     */
    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + departmentList.hashCode();
        return result;
    }
}
