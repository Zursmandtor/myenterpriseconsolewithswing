package com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.department.impl;

import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.department.Department;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.employee.Employee;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Type of department SEO
 *
 * @author Rudenko Ievgen
 */
@Entity
@DiscriminatorValue("SeoDisc")
public class Seo extends Department implements Serializable {

    /**
     * Creates department using name and list of employee
     *
     * @param name         name of the department
     * @param employeeList list of employee
     */
    public Seo(int id, String name, List<Employee> employeeList) {
        super(id, name, employeeList);
    }


}
