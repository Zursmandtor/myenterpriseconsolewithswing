package com.itstep.spv12v1.rudenkoEnterprise.windowInterface;


import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.DepartmentDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.EmployeeDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.EnterpriseDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.department.Department;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.employee.Employee;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.Enterprise;
import com.itstep.spv12v1.rudenkoEnterprise.utils.HibernateUtils;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Main window of app
 *
 * @author Rudenko Ievgen
 */
public class MainWindow {

    /**
     * Create`s enterpriseDAO
     */
    EnterpriseDAO enterpriseDAO;

    /**
     * Create`s departmentDAO
     */
    final DepartmentDAO departmentDAO;

    /**
     * Create`s empoyeeDAO
     */
    final EmployeeDAO employeeDAO;

    /**
     * Create`s converter of type
     */
    ConverterOfType converterOfType;


    /**
     * Constructor of main window with param
     *
     * @param enterpriseDAO
     * @param departmentDAO
     * @param employeeDAO
     * @param converterOfType
     */
    public MainWindow(
                      final EnterpriseDAO enterpriseDAO,
                      final DepartmentDAO departmentDAO,
                      final EmployeeDAO employeeDAO,
                      final ConverterOfType converterOfType){


        this.enterpriseDAO = enterpriseDAO;
        this.departmentDAO = departmentDAO;
        this.employeeDAO = employeeDAO;
        this.converterOfType = converterOfType;


        /**
         * List of all enterprises
         */
        final List<Enterprise> listEnterprise = enterpriseDAO.findAll();

        /**
         * List of department
         */
        final List<Department> listDepartment = new ArrayList<Department>();

        /**
         * Main frame of app
         */
        final JFrame jFrame = new JFrame("My enterprises");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLocationRelativeTo(null);
        jFrame.setSize(800, 300);
        jFrame.setResizable(false);

        JSplitPane mainSplitPaine = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);

        /**
         * Right panel with two buttons to operation with enterprise
         */
        JPanel rightPanel = new JPanel();
        JButton createEnterprise = new JButton ("Create enterprise");
        JButton deleteEnterprise = new JButton ("Delete enterprise");
        rightPanel.add (createEnterprise);
        rightPanel.add (deleteEnterprise);

        /**
         * Enterprise's panel
         */
        JPanel leftPanel = new JPanel(new GridLayout(3, 1, 5, 5));

        /**
         * Top of enterprise's panel
         */
        JPanel north = new JPanel();

        /**
         * Down of enterprise's panel
         */
        JPanel south = new JPanel();

        /**
         * Center of enterprise's panel
         */
        JPanel center = new JPanel();

        /**
         * Panel to select the enterprise
         */
        JPanel comboBox = new JPanel(new GridLayout(2, 2, 5, 5));

        /**
         * Combobox with all enterprise
         */
        final JComboBox selectEnterprise = new JComboBox(listEnterprise.toArray());
        JLabel labelSelectEnterprise = new JLabel("Select enterprise");
        comboBox.add(labelSelectEnterprise);
        comboBox.add(selectEnterprise);
        north.add(comboBox, BorderLayout.NORTH);

        /**
         * Panel to operation with department and employee
         */
        JPanel lists = new JPanel(new GridLayout(2, 2, 5, 5));

        /**
         * Combobox with departments of selected enterprise
         */
        final JLabel listDepartmentLabel = new JLabel("Departments");
        final JComboBox  listDepartmentCombo = new JComboBox();
        lists.add(listDepartmentLabel);
        lists.add(listDepartmentCombo);

        /**
         * Combobox with employees of selected department
         */
        JLabel listEmployeeLabel = new JLabel("Employees");
        final JComboBox listEmployeeCombo = new JComboBox();
        lists.add(listEmployeeLabel);
        lists.add(listEmployeeCombo);
        center.add(lists, BorderLayout.CENTER);

        /**
         * Panel with button to operation with department and employee
         */
        JPanel rightSideButtons = new JPanel(new GridLayout(2, 2, 5, 5));

        /**
         * Button to create department
         */
        final JButton createDepartment = new JButton ("Create department");

        /**
         * Button to delete department
         */
        JButton deleteDepartment = new JButton ("Delete department");

        /**
         * Button to create employee
         */
        JButton createEmployee = new JButton ("Create employee");

        /**
         * Button to delete employee
         */
        JButton deleteEmployee = new JButton ("Delete employee");

        /**
         * Add element to panel
         */
        rightSideButtons.add(createDepartment);
        rightSideButtons.add(deleteDepartment);
        rightSideButtons.add(createEmployee);
        rightSideButtons.add(deleteEmployee);
        south.add(rightSideButtons, BorderLayout.SOUTH);
        leftPanel.add(north);
        leftPanel.add(center);
        leftPanel.add(south);
        mainSplitPaine.setRightComponent(leftPanel);
        mainSplitPaine.setLeftComponent(rightPanel);
        mainSplitPaine.setResizeWeight(0.5);
        mainSplitPaine.setContinuousLayout(true);
        jFrame.add (mainSplitPaine);
        jFrame.setVisible(true);

        /**
         * ActionListener for the button create enterprise
         */
        createEnterprise.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new CreateEnterpriseWindow(enterpriseDAO, converterOfType, selectEnterprise, listEnterprise);

            }
        });

        /**
         * ActionListener for the button delete enterprise
         */
        deleteEnterprise.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DeleteEnterpriseWindow(enterpriseDAO, listEnterprise, selectEnterprise);
            }
        });

        /**
         * ActionListener for the combobox select enterprise
         */
        selectEnterprise.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Enterprise currentEnterprise = (Enterprise) selectEnterprise.getSelectedItem();
                List <Department> departmentList = departmentDAO.findByEnterprise(currentEnterprise);
                    listDepartmentCombo.removeAllItems();
                for (Department dep: departmentList){
                    listDepartmentCombo.addItem(dep.toString());
                }
            }
        });


        /**
         * ActionListener for add department of select enterprise to combobox
         */
        listEmployeeCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Department currentDepartment = (Department) listDepartmentCombo.getSelectedItem();
                List <Employee> employeeList = employeeDAO.findByDepartment(currentDepartment);
                listEmployeeCombo.removeAllItems();
                for (Employee emp: employeeList){
                    listEmployeeCombo.addItem(emp);
                }
            }
        });


        /**
         * Create window for add enterprise
         */
        createEnterprise.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new CreateEnterpriseWindow(enterpriseDAO, converterOfType, selectEnterprise, listEnterprise);

            }
        });

        /**
         * Create window for add employee
         */
        createEmployee.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

               new CreateEmployeeWindow(departmentDAO, employeeDAO, listEmployeeCombo, converterOfType, listEnterprise, listDepartment, listDepartmentCombo);

            }
        });

        /**
         * Create window for add department
         */
        createDepartment.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new CreateDepartmentWindow (departmentDAO,  listEnterprise, listDepartmentCombo);

            }
        });

        /**
         * Create window for remove employee
         */
        deleteEmployee.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                new DeleteEmployeeWindow(departmentDAO, employeeDAO, listEnterprise, listEmployeeCombo);
            }
        });

        /**
         * Create window for remove department
         */
        deleteDepartment.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DeleteDepartmentWindow(departmentDAO, listEnterprise, listDepartmentCombo);
            }
        });
    }


    /**
     * Start app
     * @param args
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                HibernateUtils hibernateUtils = new HibernateUtils();

                new MainWindow (

                        new EnterpriseDAO(hibernateUtils),
                        new DepartmentDAO(hibernateUtils),
                        new EmployeeDAO(hibernateUtils),
                        new ConverterOfType());
                }

        });
    }
}
