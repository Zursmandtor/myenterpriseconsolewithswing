package com.itstep.spv12v1.rudenkoEnterprise.windowInterface;


import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.DepartmentDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.EmployeeDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.department.Department;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.employee.Employee;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.Enterprise;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


/**
 * Window for remove employee
 *
 * @author Rudenko Ievgen
 */
public class DeleteEmployeeWindow {

    DepartmentDAO departmentDAO;
    EmployeeDAO employeeDAO;
    List<Enterprise> listSelectEnterprise;
    JComboBox listEmployeeCombo;


    public DeleteEmployeeWindow(final DepartmentDAO departmentDAO,
                                final EmployeeDAO employeeDAO,
                                final List listSelectEnterprise,
                                final JComboBox jListEmployee) {

        this.departmentDAO = departmentDAO;
        this.employeeDAO = employeeDAO;
        this.listSelectEnterprise = listSelectEnterprise;
        this.listEmployeeCombo = jListEmployee;


        /**
         * Main frame of window
         */
        final JFrame jFrame = new JFrame("Удаление работника");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLocationRelativeTo(null);
        jFrame.setSize(500, 300);
        jFrame.setResizable(false);

        /**
         * Main panel
         */
        final JPanel mainPanel = new JPanel(new GridLayout(2, 1, 5, 5));
        JPanel jPanelField = new JPanel(new GridLayout(6, 1, 5, 5));

        /**
         * Field to select enterprise
         */
        JLabel jLabelSelectEnterprise = new JLabel("Select enterprise");
        final JComboBox jComboBoxSelectEnterprise = new JComboBox(listSelectEnterprise.toArray());

        /**
         * Field to select department
         */
        JLabel jLabelSelectDepartment = new JLabel("Select department");
        final JComboBox jComboBoxSelectDepartment = new JComboBox();
        JLabel jLabelSelectEmployee = new JLabel("Select employee");
        final JComboBox jComboBoxSelectEmployee = new JComboBox();

        jPanelField.add(jLabelSelectEnterprise);
        jPanelField.add(jComboBoxSelectEnterprise);
        jPanelField.add(jLabelSelectDepartment);
        jPanelField.add(jComboBoxSelectDepartment);
        jPanelField.add(jLabelSelectEmployee);
        jPanelField.add(jComboBoxSelectEmployee);

        /**
         * Panel with a buttons
         */
        JPanel jPanelButton = new JPanel();
        JButton jButtonOk = new JButton("Ok");
        final JButton jButtonCansel = new JButton("Cancel");
        jPanelButton.add(jButtonOk);
        jPanelButton.add(jButtonCansel);
        mainPanel.add(jPanelField);
        mainPanel.add(jPanelButton);


        /**
         * ActionListener for the combobox select enterprise
         */
        jComboBoxSelectEnterprise.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Enterprise currentEnterprise = (Enterprise) jComboBoxSelectEnterprise.getSelectedItem();
                final List<Department> listSelectDepartment = departmentDAO.findByEnterprise(currentEnterprise);
                jComboBoxSelectDepartment.removeAllItems();
                for (Department dep : listSelectDepartment) {
                    jComboBoxSelectDepartment.addItem(dep.toString());
                }

            }
        });

        /**
         * ActionListener for the combobox select department
         */
        jComboBoxSelectDepartment.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Department currentDepartment = (Department) jComboBoxSelectDepartment.getSelectedItem();
                List<Employee> listOfEmployee = employeeDAO.findByDepartment(currentDepartment);
                jComboBoxSelectEmployee.removeAllItems();
                for (Employee emp : listOfEmployee) {
                    jComboBoxSelectEmployee.addItem(emp.toString());
                }
            }
        });

        /**
         * ActionListener for the button delete select employee
         */
        jButtonOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Employee currentEmployee = (Employee) jComboBoxSelectEmployee.getSelectedItem();
                employeeDAO.removeEmployee(currentEmployee);
                DefaultListModel model = (DefaultListModel) jListEmployee.getModel();
                model.removeElement(currentEmployee);
                jFrame.dispose();

            }
        });

        /**
         * ActionListener for the button cancel
         */
        jButtonCansel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jFrame.dispose();
            }
        });

        jFrame.add(mainPanel);
        jFrame.setVisible(true);


    }
}
