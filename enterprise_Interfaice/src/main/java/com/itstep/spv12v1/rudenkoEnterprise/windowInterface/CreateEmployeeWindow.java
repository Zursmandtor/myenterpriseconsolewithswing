package com.itstep.spv12v1.rudenkoEnterprise.windowInterface;


import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.DepartmentDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.EmployeeDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.department.Department;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.employee.Employee;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.Enterprise;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Window for create employee
 *
 * @author Rudenko Ievgen
 */
public class CreateEmployeeWindow {

    DepartmentDAO departmentDAO;
    EmployeeDAO employeeDAO;
    JComboBox listEmployeeCombo;
    ConverterOfType converterOfType;
    List<Enterprise> enterpriseList;
    List<Department> departmentList;
    JComboBox listDepartmentCombo;


    /**
     * Constructor of window for create employee with params
     *
     * @param departmentDAO
     * @param employeeDAO
     * @param listEmployeeCombo
     * @param converterOfType
     * @param enterpriseList
     * @param departmentList
     * @param listDepartmentCombo
     */
    public CreateEmployeeWindow(final DepartmentDAO departmentDAO,
                                final EmployeeDAO employeeDAO,
                                final JComboBox listEmployeeCombo,
                                final ConverterOfType converterOfType,
                                final List enterpriseList,
                                final List departmentList,
                                final JComboBox listDepartmentCombo) {

        this.departmentDAO = departmentDAO;
        this.employeeDAO = employeeDAO;
        this.listEmployeeCombo = listEmployeeCombo;
        this.converterOfType = converterOfType;
        this.enterpriseList = enterpriseList;
        this.departmentList = departmentList;
        this.listDepartmentCombo = listDepartmentCombo;

        String[] typeOfEmployee = {
                "Boss",
                "Manager",
                "Employee"
        };

        /**
         * Main frame of window
         */
        final JFrame jFrame = new JFrame("Adding employee");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLocationRelativeTo(null);
        jFrame.setSize(300, 400);
        jFrame.setResizable(false);

        /**
         * Main panel
         */
        JPanel mainPanel = new JPanel(new GridLayout(2, 1, 5, 5));
        JPanel jPanelField = new JPanel(new GridLayout(6, 1, 5, 5));

        /**
         * Field to select enterprise
         */
        JLabel jLabelSelectEnterprise = new JLabel("Select enterprise");
        final JComboBox jComboBoxSelectEnterprise = new JComboBox(enterpriseList.toArray());
        jPanelField.add(jLabelSelectEnterprise);
        jPanelField.add(jComboBoxSelectEnterprise);

        /**
         * Field to select department
         */
        JLabel jLabelSelectDepartment = new JLabel("Select department");
        final JComboBox jComboBoxSelectDepartment = new JComboBox(departmentList.toArray());
        jPanelField.add(jLabelSelectDepartment);
        jPanelField.add(jComboBoxSelectDepartment);

        /**
         * Field to enter name of employee
         */
        JLabel jLabelName = new JLabel("Enter name of employee: ");
        final JTextField jTextFieldName = new JTextField();
        jPanelField.add(jLabelName);
        jPanelField.add(jTextFieldName);

        /**
         * Field to enter age of employee
         */
        JLabel jLabelAge = new JLabel("Enter age of employee: ");
        final JTextField jTextFieldAge = new JTextField();
        jPanelField.add(jLabelAge);
        jPanelField.add(jTextFieldAge);

        /**
         * Field to enter type of employee
         */
        JLabel jLabelType = new JLabel("Select type of employee: ");
        final JComboBox jListType = new JComboBox(typeOfEmployee);
        jPanelField.add(jLabelType);
        jPanelField.add(jListType);


        /**
         * Panel with a buttons
         */
        JPanel jPanelButton = new JPanel();
        JButton jButtonOk = new JButton("Ok");
        JButton jButtonCansel = new JButton("Cancel");
        jPanelButton.add(jButtonOk);
        jPanelButton.add(jButtonCansel);
        mainPanel.add(jPanelField);
        mainPanel.add(jPanelButton);

        /**
         * Create combobox model
         */
        final DefaultComboBoxModel model = new DefaultComboBoxModel();


        /**
         * ActionListener for the button create new employee
         */
        jButtonOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = jTextFieldName.getText();
                int age = Integer.parseInt(jTextFieldAge.getText());
                String stringOfType = (String) jListType.getSelectedItem();
                int type = 0;
                try {
                    type = converterOfType.convertTypeOfEmployee(stringOfType);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

                Enterprise currentEnterprise = (Enterprise) jComboBoxSelectEnterprise.getSelectedItem();
                Department currentDepartment = (Department) jComboBoxSelectDepartment.getSelectedItem();
                Employee employee = new Employee(0, name, age, type);
                employeeDAO.saveEmployeeToDepartment(currentEnterprise, currentDepartment, employee);
                listEmployeeCombo.addItem(employee);
                jFrame.dispose();

            }
        });

        /**
         * ActionListener for the button cancel
         */
        jButtonCansel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jFrame.dispose();
            }
        });

        jFrame.add(mainPanel);
        jFrame.setVisible(true);


    }

}


