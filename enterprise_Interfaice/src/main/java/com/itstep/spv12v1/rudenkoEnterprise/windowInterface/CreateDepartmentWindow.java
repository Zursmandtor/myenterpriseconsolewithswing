package com.itstep.spv12v1.rudenkoEnterprise.windowInterface;


import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.DepartmentDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.department.Department;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.employee.Employee;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.Enterprise;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Window for create department
 *
 * @author Rudenko Ievgen
 */
public class CreateDepartmentWindow {

    DepartmentDAO departmentDAO;
    List listEnterprise;
    JComboBox listDepartmentCombo;


    /**
     * Constructor of window for create department with params
     *
     * @param departmentDAO
     * @param listEnterprise
     * @param departmentList
     */
    public CreateDepartmentWindow(final DepartmentDAO departmentDAO,
                                  final List listEnterprise,
                                  final JComboBox departmentList) {

        this.departmentDAO = departmentDAO;
        this.listEnterprise = listEnterprise;
        this.listDepartmentCombo = departmentList;


        /**
         * Main frame of window
         */
        final JFrame jFrame = new JFrame("Adding department");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLocationRelativeTo(null);
        jFrame.setSize(400, 200);
        jFrame.setResizable(false);

        /**
         * Main panel
         */
        JPanel mainPanel = new JPanel();
        JPanel jPanelField = new JPanel(new GridLayout(6, 1, 5, 5));

        /**
         * Field to enter name of enterprise
         */
        JLabel jLabelName = new JLabel("Enter name of department : ");
        final JTextField jTextFieldName = new JTextField();
        jPanelField.add(jLabelName);
        jPanelField.add(jTextFieldName);

        /**
         * Field to select enterprise
         */
        JLabel jLabelSelectEnterprise = new JLabel("Select enterprise : ");
        final JComboBox jComboBoxSelectEnterprise = new JComboBox(listEnterprise.toArray());
        jPanelField.add(jLabelSelectEnterprise);
        jPanelField.add(jComboBoxSelectEnterprise);


        /**
         * Panel with a buttons
         */
        JPanel jPanelButton = new JPanel();
        JButton jButtonOk = new JButton("Ok");
        JButton jButtonCansel = new JButton("Cancel");
        jPanelButton.add(jButtonOk);
        jPanelButton.add(jButtonCansel);
        mainPanel.add(jPanelField);
        mainPanel.add(jPanelButton);


        /**
         * ActionListener for the button create new department
         */
        jButtonOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = jTextFieldName.getText();
                java.util.List<Employee> employeeList = null;
                Department department = new Department(0, name, employeeList);
                Enterprise currentEnterprise = (Enterprise) jComboBoxSelectEnterprise.getSelectedItem();
                departmentDAO.saveDepartmentToEnterprise(currentEnterprise, department);
                listDepartmentCombo.addItem(department);
                jFrame.dispose();

            }
        });

        /**
         * ActionListener for the combobox select enterprise
         */
        jComboBoxSelectEnterprise.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Enterprise currentEnterprise = (Enterprise) jComboBoxSelectEnterprise.getSelectedItem();
                List<Department> departmentList = departmentDAO.findByEnterprise(currentEnterprise);
                listDepartmentCombo.removeAllItems();
                for (Department dep : departmentList) {
                    listDepartmentCombo.addItem(dep.toString());
                }
            }
        });

        /**
         * ActionListener for the button cancel
         */
        jButtonCansel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jFrame.dispose();
            }
        });

        jFrame.add(mainPanel);
        jFrame.setVisible(true);

    }

}
