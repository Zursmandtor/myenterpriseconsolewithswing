package com.itstep.spv12v1.rudenkoEnterprise.windowInterface;


import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.EnterpriseDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.Enterprise;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


/**
 * Window for create enterprise
 *
 * @author Rudenko Ievgen
 */
public class CreateEnterpriseWindow {

    EnterpriseDAO enterpriseDAO;
    ConverterOfType converterOfType;
    JComboBox selectEnterprise;
    List<Enterprise> listOfEnterprise;


    public CreateEnterpriseWindow(final EnterpriseDAO enterpriseDAO,
                                  final ConverterOfType converterOfType,
                                  final JComboBox selectEnterprise,
                                  final List<Enterprise> listOfEnterprise) {
        this.enterpriseDAO = enterpriseDAO;
        this.converterOfType = converterOfType;
        this.selectEnterprise = selectEnterprise;
        this.listOfEnterprise = listOfEnterprise;

        String[] typeOfEnterprise = {
                "Mining enterprise",
                "Music store",
        };


        /**
         * Main frame of window
         */
        final JFrame jFrame = new JFrame("Adding enterprise");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLocationRelativeTo(null);
        jFrame.setSize(500, 300);
        jFrame.setResizable(false);

        /**
         * Main panel
         */
        JPanel mainPanel = new JPanel(new GridLayout(2, 1, 5, 5));
        JPanel jPanelField = new JPanel(new GridLayout(6, 1, 5, 5));

        /**
         * Field to select type of enterprise
         */
        JLabel jLabelType = new JLabel("Select type of enterprise: ");
        final JComboBox jListType = new JComboBox(typeOfEnterprise);
        jPanelField.add(jLabelType);
        jPanelField.add(jListType);

        /**
         * Field to enter name of enterprise
         */
        JLabel jLabelName = new JLabel("Select name of enterprise: ");
        final JTextField jTextFieldName = new JTextField();
        jPanelField.add(jLabelName);
        jPanelField.add(jTextFieldName);


        /**
         * Panel with a buttons
         */
        JPanel jPanelButton = new JPanel();
        JButton jButtonOk = new JButton("Ok");
        JButton jButtonCansel = new JButton("Cancel");
        jPanelButton.add(jButtonOk);
        jPanelButton.add(jButtonCansel);
        mainPanel.add(jPanelField);
        mainPanel.add(jPanelButton);

        /**
         * ActionListener for the button create new enterprise
         */
        jButtonOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = jTextFieldName.getText();
                String stringOfType = (String) jListType.getSelectedItem();
                int type = 0;
                try {
                    type = converterOfType.convertTypeOfEnterprise(stringOfType);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

                Enterprise currentEnterprise = new Enterprise(name, type);
                enterpriseDAO.saveEnterprise(currentEnterprise);
                listOfEnterprise.add(currentEnterprise);
                selectEnterprise.addItem(currentEnterprise);
                jFrame.dispose();

            }
        });

        /**
         * ActionListener for the button cancel
         */
        jButtonCansel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jFrame.dispose();
            }
        });

        jFrame.add(mainPanel);
        jFrame.setVisible(true);

    }
}
