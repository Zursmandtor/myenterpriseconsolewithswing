package com.itstep.spv12v1.rudenkoEnterprise.consoleInterface.interf;

import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.DepartmentDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.EmployeeDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.EnterpriseDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.department.Department;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.employee.Employee;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.employee.impl.Director;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.employee.impl.Manager;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.employee.impl.Worker;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.Enterprise;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.impl.MiningEnterprise;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.impl.MusicStore;
import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * User`s console interfaice
 */
public class Menu {

    /**
     * Create`s logger
     */
    private static final Logger log = Logger.getLogger(Menu.class);


    /**
     * Create`s scanner
     */
    private Scanner scanner;

    /**
     * Create`s departmentDAO
     */
    private DepartmentDAO departmentDAO;

    /**
     * Create`s empoyeeDAO
     */
    private EmployeeDAO employeeDAO;

    /**
     * Create`s enterpriseDAO
     */
    private EnterpriseDAO enterpriseDAO;


    /**
     * Create`s enterprise
     */
    private Enterprise enterprise;

    /**
     * Create`s department
     */
    private Department department;

    /**
     * Create`s employee
     */
    private Employee employee;



    /**
     * Create`s menu
     *
     * @param scanner       scanner that to read user`s text
     * @param employeeDAO   employeeDAO
     * @param departmentDao departmentDAO
     * @param enterpriseDAO enterpriseDAO
     */
    public Menu(Scanner scanner,
                EmployeeDAO employeeDAO,
                DepartmentDAO departmentDao,
                EnterpriseDAO enterpriseDAO) {
        this.scanner = scanner;
        this.employeeDAO = employeeDAO;
        this.departmentDAO = departmentDao;
        this.enterpriseDAO = enterpriseDAO;
    }


    /**
     * Container with messages for user
     */
    public static Properties message;

    /**
     * Main method connection user and application
     *
     * @throws FileNotFoundException
     */
    public void start() {
        readLanguageMessages();
        scanner = new Scanner(System.in);

        System.out.println(message.getProperty("GREETINGS"));
        System.out.println(message.getProperty("MENU"));

        while (true) {
            int choiceInt = Integer.parseInt(getChoice("."));
            switch (choiceInt) {
                case 1:
                    createEnterprise();
                    break;
                case 2:
                    deleteEnterprise();
                    break;
                case 3:
                    showEnterprises();
                    break;
                case 4:
                    createDepartment();
                    break;
                case 5:
                    deleteDepartment();
                    break;
                case 6:
                    createEmployee();
                    break;
                case 7:
                    deleteEmployee();
                    break;
                case 8:
                    System.exit(0);

                default:
                    break;
            }
        }
    }

    /**
     * Reading messages for user
     */
    private void readLanguageMessages() {
        try {
            message = new Properties();
            message.load(new InputStreamReader(getClass().getResourceAsStream("/russianMessages.properties"), "Cp1251"));
            log.debug("File with language strings was read.");
        } catch (FileNotFoundException e) {
            log.error("File was not found.", e);
        } catch (IOException ioe) {
            ioe.printStackTrace();
            log.error("Error into stream into program.", ioe);
        }
    }

    /**
     * Method for creating enterprise
     */
    private void createEnterprise() {
        System.out.println(message.getProperty("BR"));
        System.out.println(message.getProperty("CREATE_ENTERPRISE_WELCOME"));
        System.out.println(message.getProperty("CREATE_ENTERPRISE_NAME"));
        String name = getChoice();
        System.out.println(message.getProperty("SELECT_ENTERPRISE_TYPE"));
        int type = Integer.parseInt(getChoice("[0-9]{1,2}"));
        Enterprise currentEnterprise = null;
        try {

            currentEnterprise = createEnterpriseType(0, name, null, type);
            System.out.println(message.getProperty("SUCCESS"));
            log.debug("enterprise was created");
            enterpriseDAO.saveEnterprise(currentEnterprise);
 //           enterprisesContainer.addEnterprise(currentEnterprise);
        } catch (Exception e) {
            System.out.println(message.getProperty("FAIL"));
            log.error("enterprise was`t created", e);
        }
    }

    /**
     * Method for creating department
     */
    private void createDepartment() {
        System.out.println(message.getProperty("BR"));
        System.out.println(message.getProperty("CREATE_DEPARTMENT_WELCOME"));
        Enterprise currentEnterprise = selectEnterprise();
        showDepartmentInEnterprise(currentEnterprise);
        System.out.println(message.getProperty("CREATE_DEPARTMENT_NAME"));
        String name = getChoice();
        Department createDepartment = new Department(name);
        try {
            departmentDAO.saveDepartmentToEnterprise(currentEnterprise, createDepartment);
//            enterprise.addDepartment(createDepartment);
            System.out.println(message.getProperty("SUCCESS"));
            log.debug("department was created");
        } catch (Exception e) {
            System.out.println(message.getProperty("FAIL"));
            log.error("department wasn`t created", e);
        }
    }

    /**
     * Method for creating employee
     */
    private void createEmployee() {
        System.out.println(message.getProperty("BR_INDEX"));
        System.out.println(message.getProperty("CREATE_EMPLOYEE_WELCOME"));
        Enterprise currentEnterprise = selectEnterprise();
        Department currentDepartment = selectDepartment(currentEnterprise);
        if (currentDepartment == null){
            log.debug("please, create department");
            return;
        }
        System.out.println(message.getProperty("CREATE_EMPLOYEE_NAME"));
        String name = getChoice();
        System.out.println(message.getProperty("SELECT_EMPLOYEE_TYPE"));
        System.out.println(message.getProperty("SHOW_EMPLOYEE_TYPE"));
        int type = Integer.parseInt(getChoice("[0-9]{1,2}"));
        System.out.println(message.getProperty("CREATE_EMPLOYEE_AGE"));
        int age = Integer.parseInt(getChoice("[0-9]{1,2}"));
        try {
            Employee createEmployee = createEmployeeType(0, name, age, type);
            System.out.println(message.getProperty("SUCCESS"));
            employeeDAO.saveEmployeeToDepartment(currentEnterprise, currentDepartment, createEmployee);
//            department.addEmployee(createEmployee);
            log.debug("employee was created");

        } catch (Exception e) {
            System.out.println(message.getProperty("FAIL"));
            log.error("employee was created", e);
        }
    }

    /**
     * Method for deleting enterprise
     */
    private void deleteEnterprise() {
        System.out.println(message.getProperty("BR"));
        showEnterprises();
        Enterprise deletingEnterprise = selectEnterprise();
        try {
            enterpriseDAO.removeEnterprise(deletingEnterprise);
 //           enterprisesContainer.deleteEnterprise(deletingEnterprise);
            System.out.println(message.getProperty("SUCCESS"));
            log.debug("enterprise was deleted");
        } catch (Exception e) {
            System.out.println(message.getProperty("FAIL"));
            log.error("enterprise was`t deleted", e);
        }
    }

    /**
     * Method for deleting department
     */
    private void deleteDepartment() {
        System.out.println(message.getProperty("BR"));
        Enterprise currentEnterprise = selectEnterprise();
        Department deletingDepartment = selectDepartment(currentEnterprise);
        if (deletingDepartment == null){
            log.debug("please, create department");
            return;
        }
        try {
            departmentDAO.removeDepartment(deletingDepartment);
     //       enterprise.deleteDepartment(deletingDepartment);
            System.out.println(message.getProperty("SUCCESS"));
            log.debug("department was deleted");

        } catch (Exception e) {
            System.out.println(message.getProperty("FAIL"));
            log.error("department was`t deleted", e);
        }
    }

    /**
     * Method for deleting employee
     */
    private void deleteEmployee() {
        System.out.println(message.getProperty("BR"));
        Enterprise currentEnterprise = selectEnterprise();
        Department currentDepartment = selectDepartment(currentEnterprise);
        if (currentDepartment == null){
            log.debug("employeeList is empty");
            return;
        }
        Employee deletingEmployee = selectEmployee(currentDepartment);
        if (deletingEmployee == null){
            return;
        }
        try {
            currentDepartment.deleteEmployee(deletingEmployee);
            departmentDAO.saveDepartmentToEnterprise(currentEnterprise, currentDepartment);
            employeeDAO.removeEmployee(deletingEmployee);
            System.out.println(message.getProperty("SUCCESS"));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(message.getProperty("FAIL"));
        }
    }

    /**
     * Method for selection enterprise
     *
     * @return selection enterprise
     */
    private Enterprise selectEnterprise() {
        System.out.println(message.getProperty("SELECT_ENTERPRISE_NUMBER"));
        List<Enterprise> enterpriseList = enterpriseDAO.findAll();

        int[] ids = new int[enterpriseList.size()];
        int i = 0;
        for (Enterprise ent : enterpriseList) {
            ids[i++] = ent.getId();
        }
        i = 1;
        for (Enterprise ent : enterpriseList) {
            System.out.println(i++ + "\t" + ent.getName());
        }
        int choice = Integer.parseInt(getChoice("[1-" + enterpriseList.size() + "]"));
        if (enterpriseList.size()==0){
            System.out.println("enterpriseList is empty. Please create enterprise");
            return null;
        }
        return enterpriseDAO.findById(ids[choice - 1]);
    }

    /**
     * Method for selection department
     *
     * @return selection department
     */
    private Department selectDepartment(Enterprise enterprise) {
        System.out.println(message.getProperty("SELECT_DEPARTMENT_NUMBER"));
        List<Department> departmentList = enterprise.getDepartmentList();

       int[] ids = new int[departmentList.size()];
        int i = 0;
        for (Department dep : departmentList) {
            ids[i++] = dep.getId();
        }
        i = 1;
        for (Department dep : departmentList) {
            System.out.println(i++ + "\t" + dep.getName());
        }
        int choice = 0;
        if (departmentList.size()==0){
            System.out.println("departmentList is empty. Please create enterprise");
            return null;
        } else{
            choice = Integer.parseInt(getChoice("[1-" + departmentList.size() + "]"));
        }
        log.debug("department was founded");
        return departmentDAO.findById(ids[choice - 1]);
    }


    /**
     * Method for selection employee
     *
     * @return selection employee
     */
    private Employee selectEmployee(Department department) {
        System.out.println(message.getProperty("SELECT_EMPLOYEE_NUMBER"));
        List<Employee> employeeList = department.getEmployeeList();
        long[] ids = new long[employeeList.size()];
        int i = 0;
        for (Employee emp : employeeList) {
            ids[i++] = emp.getId();
        }
        i = 1;
        for (Employee emp : employeeList) {
            System.out.println(i++ + "\t" + emp.getName());
        }
        int choice = 0;
        if (employeeList.size() == 0){
            System.out.println("employeeList is empty. Please create enterprise");
            return null;
        } else{
        choice = Integer.parseInt(getChoice("[1-" + employeeList.size() + "]"));

        }
        log.debug("employee was founded");
        return employeeList.get(choice - 1);
    }

    /**
     * Regular expression that checks any number of any characters
     *
     * @return
     */
    private String getChoice() {

        log.debug("valid data");
        return getChoice(".*");
    }

    /**
     * The logic of the data request from the user and validate the string
     *
     * @param regex regulary
     * @return request from the user`s data
     */
    private String getChoice(String regex) {
        String choice;

        while (true) {
            choice = scanner.nextLine();
            if (choice.matches(regex)) {
                break;
            }
        }

        log.debug("valid data");
        return choice;
    }

    /**
     * Method for creating enterprise one of the type
     *
     * @param id             enterprise`s id
     * @param name           enterprise`s name
     * @param departmentList list of enterprise`s department
     * @param enterpriseType enterprise`s type
     * @return new enterprise
     * @throws Exception
     */
    private Enterprise createEnterpriseType(int id, String name, List<Department> departmentList, int enterpriseType) throws Exception {
        switch (enterpriseType) {
            case 1:
                log.debug("was created MiningEnterprise");
                return new MiningEnterprise(id, name, departmentList, enterpriseType);
            case 2:
                log.debug("was created MusicStore");
                return new MusicStore(id, name, departmentList, enterpriseType);
            default:
                log.debug("Can not define enterprise type");
                throw new Exception("Can not define enterprise type");
        }
    }

    /**
     * Method for creating employee one of the type
     * @param id             employee`s id
     * @param name           employee`s name
     * @param age            employee`s age
     * @param employeeType   employee`s type
     * @return               new employee
     * @throws Exception
     */
    private Employee createEmployeeType(int id, String name, int age, int employeeType) throws Exception {
        switch (employeeType) {
            case 1:
                log.debug("was created Director");
                return new Director(id, name, age, employeeType);
            case 2:
                log.debug("was created Manager");
                return new Manager(id, name, age, employeeType);
            case 3:
                log.debug("was created Worker");
                return new Worker(id, name, age, employeeType);
            default:
                throw new Exception("Can not define employee type");
        }
    }

    /**
     * Writes onto screen all enterprises
     */
    private void showEnterprises() {
        long counter = 1;
        for (Enterprise currentEnterprise : enterpriseDAO.findAll()) {
            System.out.println("    " + counter + ". " + currentEnterprise.getName());
        }
    }

    /**
     * Writes onto screen all department
     */
    private void showDepartmentInEnterprise(Enterprise selectEnterprise) {
        long counter = 1;
        for (Department currentDepartment : departmentDAO.findByEnterprise(selectEnterprise)) {
            System.out.println("    " + counter + ". " + currentDepartment.getName());
        }
    }

    /**
     * Writes onto screen all employee
     */
    private void showEmployeeinDepartment(Department selectDepartment ) {
        long counter = 1;

        for (Employee currentEmployee : employeeDAO.findByDepartment(selectDepartment)) {
            System.out.println("    " + counter + ". " + currentEmployee.getName());
        }
    }
}