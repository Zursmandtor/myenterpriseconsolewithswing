package com.itstep.spv12v1.rudenkoEnterprise;



import com.itstep.spv12v1.rudenkoEnterprise.consoleInterface.interf.Menu;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.DepartmentDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.EmployeeDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.EnterpriseDAO;
import com.itstep.spv12v1.rudenkoEnterprise.utils.HibernateUtils;

import java.lang.String;
import java.lang.System;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        HibernateUtils hibernateUtils = new HibernateUtils();
        try {



            Menu menu = new Menu(
                    new Scanner(System.in),
                    new EmployeeDAO(hibernateUtils),
                    new DepartmentDAO(hibernateUtils),
                    new EnterpriseDAO(hibernateUtils));

            menu.start();


        } catch (Throwable ex){
            System.out.println("Unknown error");
        }
        finally {
            hibernateUtils.closeSession();
        }

    }


}


