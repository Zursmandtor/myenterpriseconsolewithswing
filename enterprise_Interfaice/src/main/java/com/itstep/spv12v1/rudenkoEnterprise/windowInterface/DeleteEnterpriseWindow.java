package com.itstep.spv12v1.rudenkoEnterprise.windowInterface;


import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.EnterpriseDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.Enterprise;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


/**
 * Window for remove enterprise
 *
 * @author Rudenko Ievgen
 */
public class DeleteEnterpriseWindow {

    EnterpriseDAO enterpriseDAO;
    List<Enterprise> listOfEnterprise;
    JComboBox selectEnterprise;


    public DeleteEnterpriseWindow(final EnterpriseDAO enterpriseDAO,
                                  final List<Enterprise> listOfEnterprise,
                                  final JComboBox selectEnterprise) {

        this.enterpriseDAO = enterpriseDAO;
        this.listOfEnterprise = listOfEnterprise;
        this.selectEnterprise = selectEnterprise;


        /**
         * Main frame of window
         */
        final JFrame jFrame = new JFrame("Deleting enterprise");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLocationRelativeTo(null);
        jFrame.setSize(400, 200);
        jFrame.setResizable(false);

        /**
         * Main panel
         */
        JPanel mainPanel = new JPanel();
        JPanel jPanelField = new JPanel(new GridLayout(6, 1, 5, 5));

        /**
         * Field to select enterprise
         */
        JLabel jLabelSelectEnterprise = new JLabel("Select enterprise: ");
        final JComboBox jComboBoxSelectEnterprise = new JComboBox(listOfEnterprise.toArray());
        jPanelField.add(jLabelSelectEnterprise);
        jPanelField.add(jComboBoxSelectEnterprise);


        /**
         * Panel with a buttons
         */
        JPanel jPanelButton = new JPanel();
        JButton jButtonOk = new JButton("Ok");
        JButton jButtonCansel = new JButton("Cancel");
        jPanelButton.add(jButtonOk);
        jPanelButton.add(jButtonCansel);
        mainPanel.add(jPanelField);
        mainPanel.add(jPanelButton);


        /**
         * ActionListener for the button delete select enterprise
         */
        jButtonOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Enterprise currentEnterprise = (Enterprise) jComboBoxSelectEnterprise.getSelectedItem();
                enterpriseDAO.removeEnterprise(currentEnterprise);
                listOfEnterprise.remove(currentEnterprise);
                jComboBoxSelectEnterprise.removeItem(currentEnterprise);
                selectEnterprise.removeItem(currentEnterprise);
                jFrame.dispose();

            }
        });

        /**
         * ActionListener for the button cancel
         */
        jButtonCansel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jFrame.dispose();
            }
        });

        jFrame.add(mainPanel);
        jFrame.setVisible(true);

    }

}
