package com.itstep.spv12v1.rudenkoEnterprise.windowInterface;


import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.DepartmentDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DAO.EmployeeDAO;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.department.Department;
import com.itstep.spv12v1.rudenkoEnterprise.entities.DTO.enterprise.Enterprise;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Window for remove department
 *
 * @author Rudenko Ievgen
 */
public class DeleteDepartmentWindow {

    DepartmentDAO departmentDAO;
    List listEnterprise;
    JComboBox listDepartmentCombo;


    public DeleteDepartmentWindow(final DepartmentDAO departmentDAO,
                                 final List listEnterprise,
                                 final JComboBox listDepartmentCombo){

        this.departmentDAO = departmentDAO;
        this.listEnterprise = listEnterprise;
        this.listDepartmentCombo = listDepartmentCombo;



        /**
         * Main frame of window
         */
        final JFrame jFrame = new JFrame("Удаление отдела");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLocationRelativeTo(null);
        jFrame.setSize(500, 300);
        jFrame.setResizable(false);

        /**
         * Main panel
         */
        final JPanel mainPanel = new JPanel(new GridLayout(2, 1, 5, 5));
        JPanel jPanelField = new JPanel(new GridLayout(6, 1, 5, 5));

        /**
         * Field to select enterprise
         */
        JLabel jLabelSelectEnterprise = new JLabel("Select enterprise");
        final JComboBox jComboBoxSelectEnterprise = new JComboBox(listEnterprise.toArray());
        jPanelField.add(jLabelSelectEnterprise);
        jPanelField.add(jComboBoxSelectEnterprise);

        /**
         * Field to select department
         */
        JLabel jLabelSelectDepartment = new JLabel("Select department");
        final JComboBox jComboBoxSelectDepartment = new JComboBox();
        jPanelField.add(jLabelSelectDepartment);
        jPanelField.add(jComboBoxSelectDepartment);

        /**
         * Panel with a buttons
         */
        JPanel jPanelButton = new JPanel();
        JButton jButtonOk = new JButton("Ok");
        final JButton jButtonCansel = new JButton("Cancel");
        jPanelButton.add(jButtonOk);
        jPanelButton.add(jButtonCansel);
        mainPanel.add(jPanelField);
        mainPanel.add(jPanelButton);


        /**
         * ActionListener for the combobox select enterprise
         */
        jComboBoxSelectEnterprise.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Enterprise currentEnterprise = (Enterprise) jComboBoxSelectEnterprise.getSelectedItem();
                final List <Department> listSelectDepartment = departmentDAO.findByEnterprise(currentEnterprise);
                jComboBoxSelectDepartment.removeAllItems();
                for (Department dep: listSelectDepartment){
                    jComboBoxSelectDepartment.addItem(dep.toString());
                }
            }
        });

        /**
         * Create deleting department
         */
        final Department[] currentDepartment = {new Department()};

        /**
         * ActionListener for the combobox select department
         */
        jComboBoxSelectDepartment.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentDepartment[0] = (Department) jComboBoxSelectDepartment.getSelectedItem();
            }
        });

        /**
         * ActionListener for the button delete select department
         */
        jButtonOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                departmentDAO.removeDepartment(currentDepartment[0]);
                DefaultListModel model = (DefaultListModel) listDepartmentCombo.getModel();
                model.removeElement(currentDepartment);
                jFrame.dispose();
            }
        });

        /**
         * ActionListener for the button cancel
         */
        jButtonCansel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jFrame.dispose();
            }
        });

        jFrame.add(mainPanel);
        jFrame.setVisible(true);


    }
}
