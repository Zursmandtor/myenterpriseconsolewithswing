package com.itstep.spv12v1.rudenkoEnterprise.windowInterface;



public class ConverterOfType {

    String nameOfType;

    public int convertTypeOfEmployee (String nameOfType) throws Exception {
        this.nameOfType = nameOfType;
        int typeOfEnterprise;
        switch (nameOfType) {
            case "Boss":
                typeOfEnterprise = 1;
                return typeOfEnterprise;

            case "Manager":
                typeOfEnterprise = 2;
                return typeOfEnterprise;

            case "Employee":
                typeOfEnterprise = 3;
                return typeOfEnterprise;

            default:
                throw new Exception("Can not define employee type");
        }
    }



    public int convertTypeOfEnterprise (String nameOfType) throws Exception {
        this.nameOfType = nameOfType;
        int typeOfEnterprise;
        switch (nameOfType) {
            case "Mining enterprise":
                typeOfEnterprise = 1;
                return typeOfEnterprise;

            case "Music store":
                typeOfEnterprise = 2;
                return typeOfEnterprise;


            default:
                throw new Exception("Can not define enterprise type");
        }
    }


}

